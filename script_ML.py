
# pour bien utiliser faire du machine learning 
import pandas as pd
import numpy as np
import math


# sklearn on ne peut pas importer toute a libraire mais on importe par classe (libraire organise oriente objet)
from sklearn.model_selection import train_test_split,cross_val_score,learning_curve
from sklearn import linear_model
from sklearn.metrics import accuracy_score,plot_confusion_matrix,confusion_matrix
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler,MinMaxScaler,RobustScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

#framework pour reduction de dimension 
import umap.umap_ as umap

# visualition graphique
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap




#########################################
#        Fonction                       #
#########################################

### Normalisation 

def compare_normalisation(s_sc,X):
    "Compare les normalisation possible via l'affichage d'un graphique"
    #Le parametre de la fonction soit la variable s_sc doit etre issu de StandardScaler réaliser en amont de la fonction
    
    # Apel des fonctions adéquatre MinMaxScaler et RobustScaler juste pour avoir une idéee des données avantapres 
    mm_sc = MinMaxScaler()
    r_sc = RobustScaler()

    #develop une fonction de transformation a partir de X train et l'applique 

    #comparatif avec donnée entrainement 
    sc_X = s_sc.fit_transform(X)
    mmc_X = mm_sc.fit_transform(X)
    r_X = r_sc.fit_transform(X)

    #construction du plot comparatif 
    plt.scatter(sc_X[:,0],sc_X[:,1],color= 'red')
    plt.scatter(mmc_X[:,0],mmc_X[:,1],color= 'orange')
    plt.scatter(r_X[:,0],r_X[:,1],color= 'green')
    plt.show()


### construction du model

def modelisation(choix):
    """Fonction réalisant une regression logistique spécifique en fonction du choix de l'utilisateur  """
    if choix == 1:
        print("Vous avec choisit regression ")
        #multi_class multinomial car nos donnée possedent plus de deux categories donc on ne peut pas utiliser le mode binaire
        #si la variable choix est de 1 alors on va utiler le model de regression classique
        model = linear_model.LogisticRegression(multi_class='multinomial')
    elif choix == 2:
        print("Vous avec choisit regression avec régulation")
        #si la variable choix est de 1 alors on va utiler le model de regression avec ajout d'un poid C
        ### Regularisation
        model = linear_model.LogisticRegression(multi_class='multinomial',C=1e3) # le C pour la régularisation 
    else:
        print("Vous avec choisit regression avec validation croisée")
        #si la variable choix est autre chose que 1 et 1 alors on lance la regression avec validation croisée
        ### Regularisation
        model = linear_model.LogisticRegressionCV(multi_class='multinomial',cv=10) # cv= 10 on relance 10fois 
    
    return model


### Visualition model regression log 

def RegLog_plot(X,y):
    """ Visualisation graphique de la regression logistique"""
    X_set, y_set = X, y
    X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                        np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))

    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c = ListedColormap(('red', 'green', 'blue','orange','pink'))(i), label = j,
                    alpha=0.3, edgecolors='none')
    plt.title('Logistic Regression (Test set)')
    plt.xlabel(' feature')
    plt.ylabel(' labels')
    plt.legend(loc="best")
    plt.show() 


### learning rate avec visualisation graphique 

def plot_learning(N,entrainement,valeur):

    #print(Nb)
    plt.plot(Nb,entrainement_score.mean(axis=1),label="Entrainement")
    plt.plot(Nb,valeur_score.mean(axis=1),label="Validation")

    plt.legend(loc="best")
    plt.xlabel("Training ")
    plt.ylabel("Score")
    plt.ylim(0.5,1.1)
    plt.show()


#########################################
#        Charger les donnees            #
#########################################

### Chargement des fichiers adéquates
x_feature = pd.read_csv('/Users/marwa/Desktop/data/data.csv',header =None)
del x_feature[0] #supprimer la premiere colonne avec marqué sample
x_feature.drop([0,0], inplace=True)  #supprimer la premiere ligne avec marqué gene

y_label  = pd.read_csv('/Users/marwa/Desktop/data/labels.csv', header=None)
del y_label[0]#supprimer la premiere colonne
y_label.drop([0,0], inplace=True)  #supprimer la premiere ligne 



# Preparation des donnees
Y = y_label.iloc[:,0]
X = x_feature.iloc[:,0:]


# split on fractionne les données en 30% test et 70% entrainement 
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33,random_state=9)
#si on utilise random_state = n avec n par exemple 7 alors on aura tjrs les meme valeurs 


#########################################
#        Normalisation                  #
#########################################

#StandardScaler 
s_sc = StandardScaler()
X_train = s_sc.fit_transform(X_train)
X_test = s_sc.transform(X_test)

#comparaison des techniques de normalisation
compare_normalisation(s_sc,X_train)


#########################################
#        Réduction de dimension         #
#########################################

#affiche le nombre de dimension des échantillons avant le processus de réductions
print("avant dimension : ",X_test.shape)


"""
# Reduction de dimension avec LDA
lda = LDA(n_components = 2) #deux couches 
X_train = lda.fit_transform(X_train, y_train)
X_test = lda.transform(X_test)



# Reduction de dimension avec PCA
pca=PCA(n_components=30) #30 couches
X_train = pca.fit_transform(X_train, y_train)
X_test = pca.transform(X_test)
"""

# Reduction de dimension avec UMAP
dim_reduction = umap.UMAP(n_components=30) #30 couches
X_train = dim_reduction.fit_transform(X_train)
X_test = dim_reduction.transform(X_test)

print("apres dimension :", X_test.shape)

########################################################################
#        Construction du model basé sur regression logistique          #
########################################################################

choix =1

#1= regression logistique
#2= regression logistique avec regularisation
#autre nombre n = regression logistique avec cross over


RLogmodel=modelisation(choix)
RLogmodel.fit(X_train, y_train) 


### Evaluation
print(RLogmodel.score(X_train, y_train))
### Prediction 

#partie sur la prediction utilisation des données non entrainer le but est que l'algo déduit les classé donc ne pas mettre les donnes d'entrainements
Z = RLogmodel.predict(X_test) # Z = les  Y predit

### Matrice de confusion
#cm = confusion_matrix(y_test, Z)

# cm mais version plot
plot_confusion_matrix(Arbremodel, X_test, y_test) 
plt.show()

### Score d'accuracy
print(accuracy_score(y_test, RLogmodel.predict(X_test))) 
print("scores test (pour voir si y'a overfitting",accuracy_score(y_train, RLogmodel.predict(X_train)))


# Use cross_val_score to automatically split, fit, and score.
# permet de connaitre le score du model avec une cross validation (cette fonction fit donc pas la peine d'exe fit en amont)
# utile quand on fait tester model avec validation croisée et poid.
scores = cross_val_score(RLogmodel, X_train, y_train, cv=10)
print('Score Validation croisee ', scores, '/n Moyenne: {}'.format(scores.mean()))

"""
Overitting :
quand on n’a par exemple un bon score a l’entrainement mais mauvais score sur la validation

"""

### Visualition modele regression log uniquement
RegLog_plot(X_test, y_test)

### learning rate avec visualisation graphique 
#mettre cv ? oui pour la validation croisé rend le model plus pertinent si besoin de test sans cv alors retirer dans les arguments de la fonctions
Nb, entrainement_score, valeur_score = learning_curve(RLogmodel, X_train, y_train,cv=10)
plot_learning(Nb,entrainement_score,valeur_score)


# SI ON NE VEUT PAS EXECUTER LE MODEL SUR L ALGO DE REGRESSION LOGISTIQUE ON L'EXE SUR L'ARBRE DE DECISIONS
# NE PAS FAIRE LES DEUX 
"""
#########################################################################
#        Arbres de decisions  UNIQUEMENT pour la comparaison            #
#########################################################################

 
#min_samples_split = nombre min d'echantillons pour un noeud = 30 au vu de la quantitée de donnée utilisé
#min_samples_leaf = le nombre min d'echantillons pour faire des noeud intermédiaire 20 sembe etre un bon compromit 
Arbremodel = DecisionTreeClassifier(min_samples_split=30,min_samples_leaf=20,splitter='best')

Arbremodel.fit(X_train, y_train)

print(Arbremodel.score(X_train, y_train))

Z = Arbremodel.predict(X_test) # Z = les  Y predit

plot_confusion_matrix(RLogmodel, X_test, y_test) 
plt.show()

print(accuracy_score(y_test, Arbremodel.predict(X_test))) 
print("scores test (pour voir si y'a overfitting",accuracy_score(y_train, Arbremodel.predict(X_train)))

scores = cross_val_score(Arbremodel, X_train, y_train, cv=10)
print('Score Validation croisee ', scores, '/n Moyenne: {}'.format(scores.mean()))


### learning rate avec visualisation graphique 

Nb, entrainement_score, valeur_score = learning_curve(Arbremodel, X_train, y_train,cv=10)
plot_learning(Nb,entrainement_score,valeur_score)
"""

