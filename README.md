## 1. Auteur
Maroua CHAHDIL  

## 2. But
Etude de donnes RNA-seq en machine learning

## 3. Protocoles



1. Téléchargement du projet via gitlab
2. DéCompression du fichier zip
3. Aller dans le dossier biostat_projetM
4. Executer le fichier python dans le logiciels adéquates (Spyder, visualCodestudio, Pycharm...)

**REMARQUE**: dans le code veuillez changer le chemin des fichiers a importer avant d'executer .



## 3. Librairies nécessaires:

- Réalisation du projet sous Mac OS Catalina Version 10.15.7 (19H15)
- Python 3.8


Permet une construction de data frame, tableau necessaire pour l'execution du code.
Egalement contient des fonctions importante pour le machine learning

`pip install nympy`

`pip install panda`

`pip install sklearn`

`pip install pip install umap-learn`

tout cela est deja installer sur l'environement conda disponible sur ce projet dans le fichier  **projet.yml**


## 4. Machinelearning.py :
Explication du fichier python contenant le code et comment l'executer

#### 4.1 Générer les données :

Telechargement des fichiers du  [site](https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq)
On obtient alors un fichier data.csv sous forme de tableau contenant tout les échantillons (20531) en X et en Y les 801 patients 
Puis un fichier labels.csv sous forme de tableau contenant les étiquettes (catégories de cancer) 


Les données veront leurs premier ligne et premier colonne supprimées pour une manipulation plus simple.


Une fois les données incrémenté dans un tableau panda :
```
Y = y_label.iloc[:,0]
X = x_feature.iloc[:,0:]
```

Alors nous pouvons utiliser les fonctions de sklearn (ces fonctions peuvent aussi etre utilisé avec des tableaux de type numpy)

Nous avons fractionner les données.

## Etape d'execution :

- Normalisation
- Réduction de dimension
    - LDA et PCA ne sont pas pertinent donc en commentaire cependant il est possible d'exécuter le modèle via ces approches cependnatil faudra mettre en commentraire la partie UMAP.
- Construction du model basé sur l'algorithme de regression liénaire
    - Régularisation
    - Validation croisée
    - Visualisation du model
    - Matrice de confusion
    - Learning rate

**ATTENTION** :
 Suite a cela, à partir de la ligne environs 245 il y a le code pour exécuter notre modele avec l'algorithme d'arbres de décision.
 Si on veut executer l'arbre de decision alors il ne faut pas exécuter la partir regression liénaire sinon les données seront faussé.


# Rapport 
Le rapport du projet est disponible [ici](https://drive.google.com/file/d/1nR0-FoQjt4jP_21BRVA-o5DIwlz6d1fD/view?usp=sharing)







